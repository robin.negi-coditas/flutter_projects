import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (_) => BreadCrumbProvider(),
      child: MaterialApp(
        title: 'Material App',
        home: const HomePage(),
        debugShowCheckedModeBanner: false,
        routes: {
          '/new': (context) => const NewBreadcrumbWidget(),
        },
      ),
    ),
  );
}

class BreadCrumb {
  bool isActive;
  final String name;
  String uuid;

  BreadCrumb({
    required this.isActive,
    required this.name,
  }) : uuid = const Uuid().v4();

  void activate() {
    isActive = true;
  }

  @override
  bool operator ==(covariant BreadCrumb other) => uuid == other.uuid;

  @override
  int get hashCode => uuid.hashCode;

  String get title => name + (isActive ? ' > ' : '');
}

class BreadCrumbProvider extends ChangeNotifier {
  final List<BreadCrumb> _items = [];
  UnmodifiableListView<BreadCrumb> get items => UnmodifiableListView(_items);
  void add(BreadCrumb breadcrumb) {
    for (final item in _items) {
      item.activate();
    }
    _items.add(breadcrumb);
    notifyListeners();
  }

  void reset() {
    _items.clear();
    notifyListeners();
  }
}

// typedef OnBreadCrumbTapped = void Function(BreadCrumb);

class BreadcrumbsWidget extends StatelessWidget {
  // final OnBreadCrumbTapped onTapped;
  final UnmodifiableListView<BreadCrumb> breadCrumbs;
  const BreadcrumbsWidget({Key? key, required this.breadCrumbs})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: breadCrumbs.map(
        (breadcrumb) {
          return Text(
            breadcrumb.title,
            style: TextStyle(
              color: breadcrumb.isActive ? Colors.blue : Colors.black,
            ),
          );
        }, // TextStyle); // Text
      ).toList(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Title'),
      ),
      body: Column(children: [
        Consumer<BreadCrumbProvider>(builder: (context, value, child) {
          return BreadcrumbsWidget(
            breadCrumbs: value.items,
          ); // BreadcrumbsWidget
        }),
        TextButton(
          onPressed: () {
            Navigator.of(context).pushNamed(
              '/new',
            );
          },
          child: const Text(
            'Add new bread crumb',
          ), // Text,],
        ),
        TextButton(
          onPressed: () {
            context.read<BreadCrumbProvider>().reset();
          },
          child: const Text(
            'Reset',
          ), // Text,],
        ),
      ]),
    );
  }
}

class NewBreadcrumbWidget extends StatefulWidget {
  const NewBreadcrumbWidget({super.key});
  @override
  State<NewBreadcrumbWidget> createState() => _NewBreadcrumbWidgetState();
}

class _NewBreadcrumbWidgetState extends State<NewBreadcrumbWidget> {
  late final TextEditingController _controller;
  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add New Breadcrumb'),
      ),
      body: Column(
        children: [
          TextField(
            controller: _controller,
            decoration: const InputDecoration(
              hintText: 'Enter a new bread crumb here...',
            ),
          ),
          TextButton(
            onPressed: () {
              final text = _controller.text;
              if (text.isNotEmpty) {
                final breadCrumb = BreadCrumb(
                  isActive: false,
                  name: text,
                ); // Bread Crumb
                context.read<BreadCrumbProvider>().add(
                      breadCrumb,
                    );
                Navigator.of(context).pop();
              }
            },
            child: const Text('Add'),
          ),
        ],
      ),
    );
  }
}
