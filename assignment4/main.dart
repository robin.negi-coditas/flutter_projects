import 'package:assignment_four/main_features_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:assignment_four/page1.dart';

void main() => runApp(
      const MaterialApp(
        title: 'Material App',
        theme: ,
        debugShowCheckedModeBanner: false,
        home: LoadingPage(),
      ),
    );

class LoadingPage extends StatefulWidget {
  const LoadingPage({super.key});

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

ThemeData _darkTheme = ThemeData(
primarySwatch: Colors.red,
brightness: Brightness. dark,);

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 3)).then((value) {
      Navigator.of(context).pushReplacement(
          CupertinoPageRoute(builder: (ctx) => MainFeaturesPage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              SizedBox(
                height: 200,
              ),
              Image(
                image: AssetImage("assets/images/logo.png"),
                width: 300,
              ),
              SizedBox(
                height: 250,
              ),
              SpinKitCircle(
                color: Color.fromARGB(255, 231, 137, 50),
                size: 80.0,
              ),
            ]),
      ),
    );
  }
}
