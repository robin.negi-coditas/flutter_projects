// import 'package:flutter/material.dart';
// import './widgets/new_transaction.dart';
// import './widgets/transaction_list.dart';
// import './models/transactions.dart';
// import './widgets/chart.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Personal Expenses',
//       theme: ThemeData(
//           primarySwatch: Colors.purple,
//           colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blueGrey),
//           fontFamily: 'Quicksand',
//           textTheme: ThemeData.light().textTheme.copyWith(
//                 titleMedium: TextStyle(
//                   fontFamily: 'Quicksand',
//                   fontWeight: FontWeight.bold,
//                   fontSize: 23,
//                 ),
//               ),
//           appBarTheme: AppBarTheme(
//               titleTextStyle: TextStyle(
//                   fontFamily: 'OpenSans',
//                   fontSize: 28,
//                   fontWeight: FontWeight.bold))),
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   // String titleInput;
//   final List<Transaction> _userTransactions = [
//     // Transaction(
//     //   id: 't1',
//     //   title: 'New Shoes',
//     //   amount: 69.99,
//     //   date: DateTime.now(),
//     // ),
//     // Transaction(
//     //   id: 't2',
//     //   title: 'Weekly Groceries',
//     //   amount: 16.53,
//     //   date: DateTime.now(),
//     // ),
//   ];
//   List<Transaction> get _recentTransactions {
//     return _userTransactions.where((tx) {
//       return tx.date.isAfter(
//         DateTime.now().subtract(
//           Duration(days: 7),
//         ),
//       );
//     }).toList();
//   }

//   void _addNewTransaction(String txTitle, double txAmount) {
//     final newTx = Transaction(
//       title: txTitle,
//       amount: txAmount,
//       date: DateTime.now(),
//       id: DateTime.now().toString(),
//     );
//     setState(() {
//       _userTransactions.add(newTx);
//     });
//   }

//   void _startAddNewTransaction(BuildContext ctx) {
//     showModalBottomSheet(
//         context: ctx,
//         builder: (_) {
//           return GestureDetector(
//             onTap: () {},
//             child: NewTransaction(_addNewTransaction),
//             behavior: HitTestBehavior.opaque,
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Flutter App'),
//         actions: [
//           IconButton(
//               onPressed: () => _startAddNewTransaction(context),
//               icon: Icon(Icons.add)),
//         ],
//       ),
//       body: SingleChildScrollView(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           children: [
//             Chart(_recentTransactions),
//             TransactionList(_userTransactions)
//           ],
//         ),
//       ),
//       floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
//       floatingActionButton: FloatingActionButton(
//           onPressed: () => _startAddNewTransaction(context),
//           child: Icon(Icons.add)),
//     );
//   }
// }

import 'package:flutter/material.dart';

class LinearProgressIndicatorClass extends StatefulWidget {
  @override
  _LinearProgressIndicatorClassState createState() =>
      _LinearProgressIndicatorClassState();
}

class _LinearProgressIndicatorClassState
    extends State<LinearProgressIndicatorClass> {
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_forward_ios_rounded),
        onPressed: () {
          setState(() {
            loading = !loading;
          });
        },
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 38),
        child: Center(
          child: loading
              ? LinearProgressIndicator()
              : Text(
                  "No task to do",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
        ),
      ),
    );
  }
}
