import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'model/random_number_model.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    int quantity = 0;
    int total = 0;

    int buyStocksQty(int currentPrice) {
      if (currentPrice < 20) {
        quantity++;
      }
      return quantity;
    }

    int totalStocks(int currentPrice) {
      if (currentPrice < 20) {
        total += currentPrice;
      }
      return total;
    }

    return ChangeNotifierProvider(
      create: (_) => RandomNumberModel(),
      child: MaterialApp(
        title: 'Material App',
        home: Scaffold(
          appBar: AppBar(
            title: const SizedBox(
              width: double.infinity,
              child: Text(
                'Portfolio',
                style: TextStyle(
                    fontSize: 32,
                    fontFamily: 'RocknRollOne-Regular',
                    fontWeight: FontWeight.normal),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          body: Consumer<RandomNumberModel>(
            builder: (context, value, child) {
              return Column(
                children: [
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(18),
                    height: 250,
                    child: Card(
                      color: Colors.green[300],
                      elevation: 5,
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          const Icon(
                            Icons.apple,
                            size: 50,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          const Text(
                            'APPLE',
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w400,
                              fontFamily: 'RocknRollOne-Regular',
                            ),
                          ),
                          const SizedBox(
                            width: 110,
                          ),
                          Text(
                            '\$ ${value.randomNumber}',
                            style: const TextStyle(
                              fontSize: 24,
                              fontFamily: 'RocknRollOne-Regular',
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 60,
                  ),
                  Center(
                    child: Text(
                      'Quantity: \$ ${buyStocksQty(value.randomNumber)}',
                      style: const TextStyle(
                        fontFamily: 'RocknRollOne-Regular',
                        fontSize: 32,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Center(
                    child: Text(
                      'Total: \$ ${totalStocks(value.randomNumber)}',
                      style: const TextStyle(
                        fontFamily: 'RocknRollOne-Regular',
                        fontSize: 32,
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
