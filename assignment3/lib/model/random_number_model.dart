import 'dart:math';
import 'package:flutter/foundation.dart';
import 'dart:async';

// class RandomNumberModel extends ChangeNotifier {
//   int _randomNumber = 0;

//   int get getRandomNumber => _randomNumber;

//   void generateRandomNumber() {
//     _randomNumber = Random().nextInt(100);
//     notifyListeners();
//   }
// }

class RandomNumberModel extends ChangeNotifier {
  late int _randomNumber = 0;
  late Timer _timer;

  RandomNumberModel() {
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      _randomNumber = Random().nextInt(100);
      notifyListeners();
    });
  }

  int get randomNumber => _randomNumber;
}
