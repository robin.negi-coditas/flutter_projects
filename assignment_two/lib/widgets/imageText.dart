import 'package:flutter/material.dart';

class ImageText extends StatelessWidget {
  final String textValue;
  final String imageSrc;
  const ImageText(this.textValue, this.imageSrc);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: 200,
      padding: EdgeInsets.all(10),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: SizedBox(
              height: 230,
              child: Image.asset(
                imageSrc,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: Text(
              textValue,
              textAlign: TextAlign.end,
              style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
