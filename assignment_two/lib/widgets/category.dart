// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

//import 'package:assignment_two/widgets/scrollPage.dart';
import 'package:flutter/material.dart';
import './imageText.dart';
import './scrollPage.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        SizedBox(
          height: 60,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            SizedBox(
              height: 150,
            ),
            Text(
              'Who are you?',
              style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            ImageText('Photographer', 'assets/images/photographer.jpg'),
            ImageText('Video Creator', 'assets/images/videoCreator.jpg'),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ImageText('Illustrator', 'assets/images/illustrator.jpg'),
            ImageText('Designer', 'assets/images/designer.jpg'),
          ],
        ),
        SizedBox(
          height: 100,
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => scrollPage()));
          },
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 18.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(40.0),
            ),
          ),
          child: Text(
            'EXPLORE NOW',
            style: TextStyle(fontSize: 20),
          ),
        )
      ],
    ));
  }
}
