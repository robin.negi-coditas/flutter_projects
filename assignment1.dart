class Product{
  String? productName;
  int? price;
  Product(this.productName,this.price);
}

abstract class Category{
  List<Product> getProducts();
  void addProducts(Product newProduct);
}

class Cart extends Category{
  List <Product> listOfProducts=[];
  @override
  getProducts(){
    return listOfProducts;
    
  }
  @override
  addProducts(product){
    listOfProducts.add(product);
    
}
}

void main(){
  Cart productList =  Cart() ;
  print(productList.listOfProducts);
  
  Product new1 = Product('product1',20);
  
  productList.addProducts(new1);
  Product new2 = Product('product2',40);
  productList.addProducts(new2);
  
  Product new3 = Product('product3',30);
  productList.addProducts(new3);
  
  print(productList.listOfProducts.map((n)=>{n.productName,n.price}).toList()) ;
  
}