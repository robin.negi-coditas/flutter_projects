import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(const CounterApp());
}

class CounterApp extends StatelessWidget {
  const CounterApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: CounterWidget(),
    );
  }
}

class CounterWidget extends StatefulWidget {
  const CounterWidget({super.key});

  @override
  State<CounterWidget> createState() => _CounterWidgetState();
}

class _CounterWidgetState extends State<CounterWidget> {
  int counterValue = 0;

  void counterFunction() {
    setState(() {
      counterValue++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('COUNTER APP'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            alignment: Alignment.center,
            child: Text(
              '$counterValue',
              style: const TextStyle(
                fontSize: 155.0,
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 35, 166, 103),
              ),
            ),
          ),
          FloatingActionButton(
              onPressed: counterFunction, child: const Icon(Icons.add)),
        ],
      ),
    );
  }
}
