// import 'package:flutter/material.dart';
// import 'package:uuid/uuid.dart';

// void main() {
//   runApp(
//     MaterialApp(
//       title: 'Material App',
//       home: const HomePage(),
//       // initialRoute: '/',
//       routes: {
//         // '/': (context) => const HomePage(),
//         '/new-contact': (context) => const NewContactView(),
//       },
//     ),
//   );
// }

// class Contact {
//   final String id;
//   final String name;
//   Contact({
//     required this.name,
//   }) : id = const Uuid().v4();
// }

// class ContactBook extends ValueNotifier<List<Contact>> {
//   //it changes only one value, list of contacts
//   ContactBook._sharedInstance() : super([]); //initial value
//   static final ContactBook _shared = ContactBook._sharedInstance();
//   factory ContactBook() => _shared;

//   //final List<Contact> _contacts = [];

//   void add({required Contact contact}) {
//     final contacts = value;
//     contacts.add(contact);
//     notifyListeners();
//   }

//   int get length => value.length;

//   void remove({required Contact contact}) {
//     final contacts = value;
//     if (contacts.contains(contact)) {
//       contacts.remove(contact);
//       notifyListeners();
//     }
//   }

//   Contact? contact({required int atIndex}) =>
//       value.length > atIndex ? value[atIndex] : null;
// }

// class HomePage extends StatelessWidget {
//   const HomePage({super.key});

//   @override
//   Widget build(BuildContext context) {
//     final contactBook = ContactBook();
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Material App Bar'),
//       ),
//       body: ValueListenableBuilder(
//           valueListenable: ContactBook(),
//           builder: (contact, value, child) {
//             final contacts = value;
//             return ListView.builder(
//               itemCount: contacts.length,
//               itemBuilder: (BuildContext context, int index) {
//                 final contact = contacts[index];
//                 return Dismissible(
//                   onDismissed: (direction) => contacts.remove(contact),
//                   key: ValueKey(contact.id),
//                   child: Material(
//                     color: Colors.white,
//                     elevation: 6.0,
//                     child: ListTile(
//                       title: Text(contact.name),
//                     ),
//                   ),
//                 );
//               },
//             );
//           }),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () async {
//           await Navigator.of(context).pushNamed('/new-contact');
//         },
//         child: const Icon(Icons.add),
//       ),
//     );
//   }
// }

// class NewContactView extends StatefulWidget {
//   const NewContactView({super.key});

//   @override
//   State<NewContactView> createState() => _NewContactViewState();
// }

// class _NewContactViewState extends State<NewContactView> {
//   late final TextEditingController _controller;
//   @override
//   void initState() {
//     _controller = TextEditingController();
//     super.initState();
//   }

//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Add New Contact'),
//       ),
//       body: Column(
//         children: [
//           TextField(
//             controller: _controller,
//             decoration: const InputDecoration(
//               hintText: 'New contact here : ',
//             ),
//           ),
//           TextButton(
//             onPressed: () {
//               final contact = Contact(name: _controller.text);
//               ContactBook().add(contact: contact);
//               Navigator.of(context).pop();
//             },
//             child: const Text('Add contact'),
//           )
//         ],
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ContactBook()),
      ],
      child: MaterialApp(
        title: 'Material App',
        home: const HomePage(),
        routes: {
          '/new-contact': (context) => const NewContactView(),
        },
      ),
    ),
  );
}

class Contact {
  final String id;
  final String name;
  Contact({
    required this.name,
  }) : id = const Uuid().v4();
}

class ContactBook extends ChangeNotifier {
  final List<Contact> _contacts = [];

  void add({required Contact contact}) {
    _contacts.add(contact);
    notifyListeners();
  }

  int get length => _contacts.length;

  void remove({required Contact contact}) {
    if (_contacts.contains(contact)) {
      _contacts.remove(contact);
      notifyListeners();
    }
  }

  Contact? contact({required int atIndex}) =>
      _contacts.length > atIndex ? _contacts[atIndex] : null;
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final contactBook = Provider.of<ContactBook>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Material App Bar'),
      ),
      body: Consumer<ContactBook>(
        builder: (context, contactBook, child) {
          final contacts = contactBook._contacts;
          return ListView.builder(
            itemCount: contacts.length,
            itemBuilder: (BuildContext context, int index) {
              final contact = contacts[index];
              return Dismissible(
                onDismissed: (direction) =>
                    contactBook.remove(contact: contact),
                key: ValueKey(contact.id),
                child: Material(
                  color: Colors.white,
                  elevation: 6.0,
                  child: ListTile(
                    title: Text(contact.name),
                  ),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.of(context).pushNamed('/new-contact');
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}

class NewContactView extends StatefulWidget {
  const NewContactView({super.key});

  @override
  State<NewContactView> createState() => _NewContactViewState();
}

class _NewContactViewState extends State<NewContactView> {
  late final TextEditingController _controller;
  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final contactBook = Provider.of<ContactBook>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add New Contact'),
      ),
      body: Column(
        children: [
          TextField(
            controller: _controller,
            decoration: const InputDecoration(
              hintText: 'New contact here : ',
            ),
          ),
          TextButton(
            onPressed: () {
              final contact = Contact(name: _controller.text);
              contactBook.add(contact: contact);
              Navigator.of(context).pop();
            },
            child: const Text('Add contact'),
          )
        ],
      ),
    );
  }
}
