import 'package:flutter/material.dart';

class Answers extends StatelessWidget {
  final VoidCallback selectHandler;
  final answerText;
  Answers({this.selectHandler, this.answerText});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
        ),
        child: Text('$answerText'),
        onPressed: selectHandler,
      ),
    );
  }
}
