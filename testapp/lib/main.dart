import 'package:flutter/material.dart';

class Counter extends InheritedNotifier {
  int _count = 0;

  Counter({required Widget child}) : super(child: child);

  int get count => _count;

  void increment() {
    _count++;
    notify();
  }

  static Counter of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Counter>()!;
  }
}






// void main() => runApp(
//       const MaterialApp(
//         title: 'Material App',
//         debugShowCheckedModeBanner: false,
//         home: HomePage(),
//       ),
//     );

// class SliderInheritedNotifier extends InheritedNotifier {
//   final double value = 0;
//   const SliderInheritedNotifier({
//     Key? key,
//     required value,
//     required Widget child,
//   }) : super(
//           key: key,
//           child: child,
//         );

//   static double of(BuildContext context) =>
//       context
//           .dependOnInheritedWidgetOfExactType<SliderInheritedNotifier>()

//       0.0;
// }

// class HomePage extends StatelessWidget {
//   const HomePage({super.key});

//   @override
//   Widget build(BuildContext context) {
//     final data = SliderInheritedNotifier.of(context);
//     return MaterialApp(
//       title: 'Material App',
//       home: Scaffold(
//         appBar: AppBar(
//           title: const Text('Material App Bar'),
//         ),
//         body: SliderInheritedNotifier(
//           value: data,
//           child: Builder(builder: (context) {
//             return Column(
//               children: [
//                 Slider(
//                   value: SliderInheritedNotifier.of(context),
//                   onChanged: (value) {
//                     data. = value;
//                   },
//                 ),
//                 Row(
//                   children: [
//                     Opacity(
//                       opacity: SliderInheritedNotifier.of(context),
//                       child: Container(
//                         color: Colors.yellow,
//                         height: 200,
//                         width: 100,
//                       ),
//                     ),
//                     Opacity(
//                       opacity: SliderInheritedNotifier.of(context),
//                       child: Container(
//                         color: Colors.blue,
//                         height: 200,
//                         width: 100,
//                       ),
//                     ),
//                   ].expandEqually().toList(),
//                 )
//               ],
//             );
//           }),
//         ),
//       ),
//     );
//   }
// }

// extension ExpandEqually on Iterable<Widget> {
//   Iterable<Widget> expandEqually() => map(
//         (w) => Expanded(
//           child: w,
//         ),
//       );
// }


//////////////////////////////////


